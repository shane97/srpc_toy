#include "websocket_srv.h"

#include <QWebSocket>

WebSocketSrv::WebSocketSrv(QObject *parent) : QObject(parent)
{

    websocket_srv_ = new QWebSocketServer(QString("Bootloader"),QWebSocketServer::NonSecureMode, this);

//    this->moveToThread(&thread_);

    qInfo() <<Q_FUNC_INFO << "ddsd";

    connect(websocket_srv_,&QWebSocketServer::newConnection,this,&WebSocketSrv::HandleNewConnection);

}

void WebSocketSrv::StartUp()
{

    qInfo() <<Q_FUNC_INFO << "ddsd";


    if(websocket_srv_->listen(QHostAddress::Any,10010))
    {
        qDebug()<<"listen:10010 succeed";
    }
    else
    {
        qDebug()<<"listen:10010 failed";
    }

}

void WebSocketSrv::HandleNewConnection()
{
    qDebug()<<"new scoket incomming";
    QWebSocket* accept_web_socket=websocket_srv_->nextPendingConnection();
//    incomming_web_socket_map_.insert(GetSocketIndex(),accept_web_socket);
    connect(accept_web_socket,&QWebSocket::disconnected,this,&WebSocketSrv::HandleSocketDisconnect);
    //connect(accept_web_socket,&QWebSocket::textFrameReceived,this,&WebSocketSrv::HandletextFrameReceived);
    connect(accept_web_socket,&QWebSocket::textMessageReceived,this,&WebSocketSrv::HandleTextMessageReceive);
}

void WebSocketSrv::HandleSocketDisconnect()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
}

void WebSocketSrv::HandleTextMessageReceive(const QString &message)
{
    qDebug() << Q_FUNC_INFO << __LINE__ << message;
}

void WebSocketSrv::HandletextFrameReceived(const QString &frame, bool isLastFrame)
{
    qDebug() << Q_FUNC_INFO << __LINE__ << frame;
}

void WebSocketSrv::HandlebinaryFrameReceived(const QByteArray &frame, bool isLastFrame)
{
    qDebug() << Q_FUNC_INFO << __LINE__ << frame;
}

void WebSocketSrv::HandlebinaryMessageReceived(const QByteArray &message)
{
    qDebug() << Q_FUNC_INFO << __LINE__ << message;
}
