#include "serviceimpl.h"

ServiceImpl::ServiceImpl()
{

}

void ServiceImpl::Echo(EchoRequest *request, EchoResponse *response, RPCContext *ctx)
{
    (void) ctx;

    response->set_message("Hi, " + request->name());
    printf("get_req:\n%s\nset_resp:\n%s\n",
            request->DebugString().c_str(), response->DebugString().c_str());
}
