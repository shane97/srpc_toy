#ifndef WEBSOCKETSRV_H
#define WEBSOCKETSRV_H

#include <QObject>

#include <QtWebSockets/QWebSocketServer>

#include <QThread>

class WebSocketSrv : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketSrv(QObject *parent = nullptr);

    void StartUp();

protected slots:
    void HandleNewConnection();

    void HandleSocketDisconnect();

    void HandleTextMessageReceive(const QString &message);

    void HandletextFrameReceived(const QString &frame, bool isLastFrame);
    void HandlebinaryFrameReceived(const QByteArray &frame, bool isLastFrame);
    void HandlebinaryMessageReceived(const QByteArray &message);

signals:

private:
    QThread thread_;

    QWebSocketServer* websocket_srv_;



};

#endif // WEBSOCKETSRV_H
