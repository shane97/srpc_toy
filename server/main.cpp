#include <QCoreApplication>

#include "workflow/WFHttpServer.h"

#include <serviceimpl.h>

#include <http_file_server.hpp>

#include <websocket_srv.h>

#include <QtConcurrent/QtConcurrent>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

//    WFHttpServer srv([](WFHttpTask *task) {
//        task->get_resp()->append_output_body("<html>Hello World!</html>");
//    });

//    if (srv.start(8888) == 0) { // start server on port 8888
//        getchar(); // press "Enter" to end.
//        srv.stop();
//    }

    WebSocketSrv web_srv;

    web_srv.StartUp();

    QtConcurrent::run([](){
        char *argvv[] = {};
        argvv[0] = "server";
        argvv[1] = "8888";
        argvv[2] = "/home/shaneluo/prog/srpc_demo/server/";
        main_http_file_srv(3, argvv);

    });


    return a.exec();
}
