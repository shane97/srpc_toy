#include <QCoreApplication>

#include "workflow/WFHttpServer.h"

#include <serviceimpl.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Example::SRPCClient client("127.0.0.1", 1412);
    EchoRequest req;
    req.set_message("Hello, srpc!");
    req.set_name("workflow");

    client.Echo(&req, [](EchoResponse *response, RPCContext *ctx) {
        if (ctx->success())
            printf("%s\n", response->DebugString().c_str());
        else
            printf("status[%d] error[%d] errmsg:%s\n",
                   ctx->get_status_code(), ctx->get_error(), ctx->get_errmsg());
    });


    return a.exec();
}
