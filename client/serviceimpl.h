#ifndef SERVICE_H
#define SERVICE_H

#include <signal.h>
#include <example.srpc.h>

using namespace srpc;

class ServiceImpl : public Example::Service
{
public:
    ServiceImpl();

    void Echo(EchoRequest *request, EchoResponse *response, srpc::RPCContext *ctx) override;

};

#endif // SERVICE_H
